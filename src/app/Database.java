package app;

/**
 * @author Wilson Peralta
 * @author Beomsik Kim
 */


import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
/**
 * 
 * Class Database keeps a list of users through the life of the program
 * it holds the user objects of the app
 *
 */
public class Database {
	
	private static List<UserSystem> users = new ArrayList<UserSystem>();  // Array which holds all the User objects that were created
	

	public static void addUser(UserSystem user) {
		
		users.add(user);
	}
	
	/**
	 * @param index location of the user to be deleted
	 */
	public static void delUser(int index) {
		
		users.remove(index);
	}
	
	/**
	 * @param index location of where to get the User
	 * @return returns the User at that location
	 */
	public static UserSystem getUser(int index) {
		
		return users.get(index);
	}
	
	public static Iterator<UserSystem> userIterator() {
		
		return users.iterator();
	}
	
	/**
	 * @throws IOException throws if file does not exist
	 */
	public static void writeUser() throws IOException {
		
		String fileName = "users.dat";
				
		ObjectOutputStream os = null;

		try {
			
			 os = new ObjectOutputStream(new FileOutputStream(fileName));
			 
			 os.writeObject(users);
			
		} catch(Exception e) {
			
			e.printStackTrace();
		}

		
		os.close();
		
	}
	/*
	 * method readUser initializes "stock" user and loads photos from stock file
	 *  
	 */
	public static void readUser() throws IOException, ParseException {
		
		String fileName = "users.dat";
		
		File file = new File("users.dat");
		
		String[] names = {"Parent", "Car", "Draw", "Death", "Eye","CarL","Bball","Muscle"};
		
		String[] fileLoc = {"src/stock/photo1.jpg", "src/stock/photo2.jpg", "src/stock/photo3.png", "src/stock/photo4.png", "src/stock/photo5.jpg",
				 "src/stock/photo6.jpg", "src/stock/photo7.jpg", "src/stock/photo8.jpg"};
		
		UserSystem.Album stockAlbum = new UserSystem.Album("stock album");
		
		if(file.length() == 0) {
			
			UserSystem stock = new UserSystem("stock");
			
			users.add(stock);
			
			for(int i = 0; i < fileLoc.length; i++) {
		
				UserSystem.Album.Photo image = new UserSystem.Album.Photo(names[i],fileLoc[i]);
				
				SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
				
				String s = sdf.format(file.lastModified());
				
				Date date = sdf.parse(s);
				
				image.setPhotoDate(date); 
				
				stockAlbum.setBeginDate(image.getPhotoDate());
				stockAlbum.setEndDate(image.getPhotoDate());
				
				if(image.getPhotoDate().before(stockAlbum.getBeginDate())) {
					
					stockAlbum.setBeginDate(image.getPhotoDate());
				}
				
				if(image.getPhotoDate().after(stockAlbum.getEndDate())) {
										
					stockAlbum.setEndDate(image.getPhotoDate());
				}
				
				stockAlbum.addPhoto(image);	
			
			}
			
			//users.add(stock);
			
			stock.addAlbum(stockAlbum);
			
			//users.add(stock);
			
			
			
			return;
		}
				
		try {
			
			ObjectInputStream in = new ObjectInputStream(new FileInputStream(fileName));
			 
	        users = (List<UserSystem>) in.readObject(); 	
	        
	        in.close();
	        
		} catch(Exception e) {
			e.printStackTrace();
		}
		
	}


}