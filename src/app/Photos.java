package app;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.stage.Stage;
import view.AdminUIController;
import view.LoginUIController;
import java.io.*;
import java.util.ArrayList;

/**
 * @author Wilson Peralta
 * @author Beomsik Kim
 */

/**
 * Class Photos initiates app
 * @param dirData for serialization
 * @param fileData for serialization
 * @param serialVersionUID for serialization
 * @param loginUI for LoginUI control
 * @param userList for list of UserSystem
 * @param mainStage for main stage
 * @param motherAnchor for main anchor
 */
public class Photos extends Application implements Serializable {
	public static final String dirData = "data";
	public static final String fileData = "userList.data";
	
	private static final long serialVersionUID = 1L;
	
	@SuppressWarnings("unused")
	private LoginUIController loginUI;
	public ArrayList<UserSystem> userList;
	private Stage mainStage;
	private AnchorPane motherAnchor;
	
	/**
	 * method storeInfo store data into file
	 * @param Photos mainObj3
	 */
	@SuppressWarnings("resource")
	public static void storeInfo(Photos mainObj3) throws IOException{
		ObjectOutputStream stream2;
		stream2 = new ObjectOutputStream(new FileOutputStream(dirData + File.separator + fileData));
		stream2.writeObject(mainObj3);
	}
	
	
	/**
	 * method retrieveInfo retrieves data into file
	 */
	@SuppressWarnings("resource")
	public static Photos retrieveInfo() throws IOException{
		ObjectInputStream stream1 = null;
		Photos mainObj2 = null;
		stream1 = new ObjectInputStream(new FileInputStream(dirData+File.separator+fileData));
		try {
			mainObj2 = (Photos)stream1.readObject();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return mainObj2;
	}
	
	/**
	 * method start starts app with givenStage
	 * @param Stage givenStage
	 */
	public void start(Stage givenStage) {
		this.mainStage = givenStage;
		try {
			FXMLLoader prep = new FXMLLoader();
			prep.setLocation(getClass().getResource("/view/LoginUI.fxml"));
			motherAnchor = (AnchorPane) prep.load();
			loginUI = prep.getController();
			
			Scene mainScene1 = new Scene(motherAnchor);
			givenStage.setTitle("Log in window");
			givenStage.setScene(mainScene1);
			givenStage.show();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	/**
	 * method main main method
	 * @param String[] args
	 */
	public static void main(String[] args) throws IOException {
		Photos mainObj=new Photos();
		if (retrieveInfo()!=null){
			mainObj = retrieveInfo();
			AdminUIController.userList1 = mainObj.userList;
		}
		else {
			//Already retrieved info
		}
		launch(args);
		mainObj.userList = AdminUIController.retrieveUL();
		storeInfo(mainObj);
	}
}