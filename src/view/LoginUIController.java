package view;


/**
 * @author Wilson Peralta
 * @author Beomsik Kim
 */
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.control.Alert.AlertType;
import view.UserSystemController;
import java.io.IOException;
import java.util.ArrayList;
import app.UserSystem;

/**
 * Class LoginUIController controls LoginUI javafx page
 * @param ul1 arraylist of UserSystem that keeps track of active users
 * @param main UserSystemController to run UserSystem
 */
public class LoginUIController{
	
	ArrayList<UserSystem> ul1 = AdminUIController.retrieveUL();
	private static UserSystem currUser;
	UserSystemController main = new UserSystemController();
	
	@FXML TextField enteredUN;
	@FXML Button login;
	
	/**
	 * method prepLogin prepares LoginUIController for main controller
	 * @param UserSystemController mainControl
	 */
	public void prepLogin(UserSystemController mainControl){
		main = mainControl;
	}
	public static int userIndex;
	/**
	 * method determineUsersIndex determines index of givenName in ua1 if found
	 * @param ArrayList<UserSystem> ua1
	 * @param String givenName
	 * @return int index of givenName in ua1 if found, -1 if not found
	 */
	public int determineUsersIndex(ArrayList<UserSystem> ua1, String givenName){
		if(ua1==null) {
			return -1;
		}
		for(int i = 0; i < ua1.size(); i++){
			if(ua1.get(i).receiveName().equals(givenName)){
				userIndex =i;
				return i;
		
			}
			
		}
		return -1;
	}
	public static int getUserIndex() {
		return userIndex;
	}
	/**
	 * method triggeredLogin logs into albumlist page if login is valid, or admin page if admin
	 * @param ActionEvent e
	 */
	public void triggeredLogin(ActionEvent e){
		String tempU = enteredUN.getText();
		if(tempU.equals("")||tempU == null){
			Alert emptyWarn=new Alert(AlertType.ERROR);
			emptyWarn.setHeaderText("Error");
			emptyWarn.setContentText("Must input a username");
			emptyWarn.show();
			return;
		}
		else if(tempU.equalsIgnoreCase("admin")){
			try {
				Stage s1=new Stage();
				FXMLLoader l1=new FXMLLoader();
				l1.setLocation(getClass().getResource("AdminUI.fxml"));
				SplitPane motherP=(SplitPane) l1.load();
				AdminUIController ac1=l1.getController();
				ac1.start();
				Scene sc2 = new Scene(motherP);
				s1.setScene(sc2);
				((Node)e.getSource()).getScene().getWindow().hide();
				s1.show();	
			} 
			catch (IOException m) {
				m.printStackTrace();
			}
		}
		else if(determineUsersIndex(ul1, tempU)==-1){
			Alert noneWarn = new Alert(AlertType.ERROR);
			noneWarn.setHeaderText("Error");
			noneWarn.setContentText("User does not exist");
			noneWarn.show();
			return;
		}
		else if(determineUsersIndex(ul1, tempU)!=-1){
			setCurrUser(ul1.get(determineUsersIndex(ul1, tempU)));
			try {
				Stage s3=new Stage();
				
				FXMLLoader l3=new FXMLLoader();
				
				l3.setLocation(getClass().getResource("Albumlist.fxml"));
				
				BorderPane motherP3=(BorderPane) l3.load();
				AlbumlistController ac3=l3.getController();
				ac3.setCurrUser(tempU);
				Scene sc3=new Scene(motherP3);
				s3.setScene(sc3);
				((Node)e.getSource()).getScene().getWindow().hide();
				s3.show();	
			} 
			catch (IOException m) {
				m.printStackTrace();
			}
		}
	}

	public static UserSystem getCurrUser() {
		return currUser;
	}

	public static void setCurrUser(UserSystem currUser) {
		LoginUIController.currUser = currUser;
	}
}