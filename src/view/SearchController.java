
package view;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.ResourceBundle;

import app.Database;
import app.Tags;
import app.UserSystem;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

/**
 * @author Wilson Peralta
 * @author Beomsik Kim
 */
public class SearchController implements Initializable{
	
	UserSystemController main = new UserSystemController();
	@FXML private ListView<UserSystem.Album.Photo> photoList;
	@FXML private ImageView photoImage;
	@FXML Button add;
	@FXML Button goBack;
	@FXML Button logout;
	
	@FXML  Label captionText, dateText, personTagText, locationTagText;
	private static ObservableList<UserSystem.Album.Photo> obsList;
	public static String personTag;
	public static String locTag;
	public static String f_dateTag;  
	public static String i_dateTag;
	
	public void initialize(URL location, ResourceBundle resources) {
		
		obsList = FXCollections.observableArrayList();
		photoList.setItems(obsList);	
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
		Date i_date = null;
		Date f_date = null;
		
		try {		
			if(i_dateTag != null)
		{		
			 i_date = sdf.parse(i_dateTag); 
		}	
		} 
		catch (ParseException e) 
		{		
			e.printStackTrace();
		}	
		try 
		{	
			if(f_dateTag != null) 
			{		
				f_date = sdf.parse(f_dateTag);	
			}
		
		} 
		catch (ParseException e) 
		{
			
			e.printStackTrace();
		}
		
		Iterator<UserSystem.Album> a_iterator = Database
				.getUser(LoginUIController.getUserIndex()).albumIterator();
		
		while(a_iterator.hasNext()) {
			
			UserSystem.Album currAlbum = a_iterator.next();
			
			Iterator<UserSystem.Album.Photo> p_iterator = currAlbum.photoIterator();
			
			while(p_iterator.hasNext()) 
			{
				
				UserSystem.Album.Photo photo = p_iterator.next();
				
				Date photoDate = photo.getPhotoDate();
				
				if(!personTag.isEmpty()) {
					
					boolean tagFound = false;
					
					Iterator<Tags> tagIter = photo.tagIterator();
					
					while(tagIter.hasNext()) 
					{
						
						Tags tag = tagIter.next();
						
						if(tag.getName().equals("person") 
								&& tag.getValue().equals(personTag)) 
						{
							
								tagFound = true;		
						}
					}
					
					if(!tagFound)
					{
						
						continue;
					}
				}	
				if(!locTag.isEmpty())
				{
					
					boolean tagFound = false;
					
					Iterator<Tags> tagIter = photo.tagIterator();
					
					while(tagIter.hasNext())
					{
						
						Tags tag = tagIter.next();
						
						if(tag.getName().equals("location") && tag.getValue().equals(locTag)) 
						{
							
								tagFound = true;		
						}
					}
					
					if(!tagFound) {
						
						continue;
					}
				
				}		
				if(i_date != null && f_date != null) {
					
					if(!(photoDate.after(i_date) && photoDate.before(f_date)) 
							&& !photoDate.equals(i_date)
							&& !photoDate.equals(f_date)) {			
						continue;
						
					}
					
				} else if(i_date != null) {
					
					if(!photoDate.after(i_date) && !photoDate.equals(i_date)) {		
						continue;
					}
					
				} else if(f_date != null) {
					
					if(!photoDate.before(f_date) && !photoDate.equals(f_date)) {
						
						continue;
					}
				}	
				obsList.add(photo);
							
			}				
			if(obsList.size() == 0) 
			{				
				blankDisplay();
			}
		}
		
		photoList.getSelectionModel()
		.selectedItemProperty()
		.addListener((obs, oldVal, newVal) -> updater());
		
		if(!obsList.isEmpty()) {
			
			photoList.getSelectionModel().select(0);
		}
	}
	
	/**
	 * @param e takes in an Action Event
	 * @throws IOException throws exception if scene cannot be loaded
	 */
	public void triggeredAdd(ActionEvent e) throws IOException {
		
		int userIndex = LoginUIController.getUserIndex();
		int i = 0;
		String albumName = AlbumlistController.addBox();
		
		if(albumName == null) { 
			
			return; 
		}
		
		UserSystem.Album album = new UserSystem.Album(albumName);
		for(UserSystem.Album.Photo p: obsList) {
			
			if(i == 0) 
			{
				
				album.setBeginDate(p.getPhotoDate());
				album.setEndDate(p.getPhotoDate());
				
			}
			
			if(p.getPhotoDate().before(album.getBeginDate())) {
				
				album.setBeginDate(p.getPhotoDate());
			}
			
			if(p.getPhotoDate().after(album.getEndDate())) {
									
				album.setEndDate(p.getPhotoDate());
			}
			
			album.addPhoto(p);	
			i++;
		}
				
		UserSystem currUser = LoginUIController.getCurrUser();
	
		currUser.addAlbum(album);
		
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(getClass().getResource("/view/Album.fxml"));
		BorderPane root = (BorderPane) loader.load();
		
		Scene scene = new Scene(root);
		Stage primaryStage = (Stage) logout.getScene().getWindow();  // gets the current stage 
		primaryStage.setScene(scene);
		primaryStage.setResizable(false);
		primaryStage.show();
			
	}
	

	public void goBack(ActionEvent e) throws IOException {
		
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(getClass().getResource("/view/Album.fxml"));
		BorderPane root = (BorderPane) loader.load();
		
		Scene scene = new Scene(root);
		Stage primaryStage = (Stage) logout.getScene().getWindow();  // gets the current stage 
		primaryStage.setScene(scene);
		primaryStage.setResizable(false);
		primaryStage.show();
		
		
	}
	
	/**
	 * @param e takes in an Action Event
	 * @throws IOException throws exception if scene cannot be loaded
	 */
	public void triggeredLogout(ActionEvent e){
		try{
			main.tgrdLogout(e);
		}
		catch(Exception r){
			r.printStackTrace();
		}
	}
	
	private void updater() {
		
		if(!obsList.isEmpty())
		{
			UserSystem.Album.Photo photo = photoList.getSelectionModel().getSelectedItem();	
			File file = new File(photo.getPhotoLocation());

			if (file.exists()) //check if photo still exists at its location on computer
			{
				Image display = new Image(file.toURI().toString());
				photoImage.setImage(display);
				captionText.setText(photo.getCaption());
				SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
				dateText.setText(sdf.format(photo.getPhotoDate()));
				personTagText.setText(photo.getTags("person"));
				locationTagText.setText(photo.getTags("location"));
				
			} 
		}
	}
	
	public void blankDisplay()
	{
		photoImage.setImage(null);
		captionText.setText("");
		dateText.setText("");
		personTagText.setText("");
		locationTagText.setText("");
	}

}