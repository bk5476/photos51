package view;

import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.Stage;
import javafx.scene.control.ButtonType;
import javafx.scene.layout.AnchorPane;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import java.util.Optional;

public class UserSystemController {
	LoginUIController loginController;
	AlbumlistController AlbumlistController;
	AdminUIController adminController;
	
	public void tgrdLogout(ActionEvent e) {
		Alert logoutWarn = new Alert(AlertType.CONFIRMATION);
		logoutWarn.setHeaderText("Confirm action");
		logoutWarn.setContentText("Log out now?");
		Optional<ButtonType> outcome = logoutWarn.showAndWait();
		if(outcome.get() == ButtonType.OK){
			((Node)e.getSource()).getScene().getWindow().hide();
			try {
				Stage s5 = new Stage();
				FXMLLoader l5 = new FXMLLoader();
				l5.setLocation(getClass().getResource("LoginUI.fxml"));
				AnchorPane motherP5 = (AnchorPane) l5.load();
				Scene sc5 = new Scene(motherP5);
				s5.setScene(sc5);
				s5.show();
			}
			catch(Exception q){
				q.printStackTrace();
			}
		}
		else{
			return;
		}
	}
	
	public void prepAll(){
		adminController.prepMain(this);
		loginController.prepLogin(this);
		AlbumlistController.prepAlbum(this);
	}
}
