package view;

import java.io.IOException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Iterator;
import java.util.Optional;
import java.util.ResourceBundle;

import app.UserSystem;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputDialog;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import javafx.util.Callback;

import view.LoginUIController;;


public class AlbumlistController implements Initializable{
	UserSystemController main = new UserSystemController();

	@FXML Button logout;
	@FXML Label titleuser;
	@FXML ListView<String> alv;
	@FXML Button add;
	@FXML Button delete;
	@FXML Button edit;
	@FXML TextField albumAdded; 
	@FXML Button search;
	@FXML Button choose;
	
	@FXML private TableView<UserSystem.Album> albumList;
	@FXML private TableColumn<UserSystem.Album,String> name;
	@FXML private TableColumn<UserSystem.Album,String> date;
	@FXML private TableColumn<UserSystem.Album,String> photo;
	
	private static ObservableList<UserSystem.Album> obsList;
	private static int openAlbumIndex = -1;
	// File myObj = new File("cookie.txt");
		 //Initialize? override
		 
		/* public void start() 
		 {
			 if(albumList1==null) {
				 albumList1=new ArrayList<UserSystem>();
			 }
			// obsList=FXCollections.observableArrayList();
			 for(int i = 0; i<albumList1.size(); i++){
				 if(albumList1.isEmpty()) break;
				 obsList.add(albumList1.get(i).receiveName());
			 }
			 albumList.getSelectionModel().select(0);
			 albumList
		 	.getSelectionModel()
		 	.selectedItemProperty();
			 albumList.setItems(obsList);
		}*/
	/**
	 * method prepAlbum prepares AlbumlistController for main controller
	 * @param UserSystemController givenMainCon
	 */
	
	public void prepAlbum(UserSystemController givenMainCon){
		main = givenMainCon;
	}
	
	public void initialize(URL arg0, ResourceBundle arg1) {
		
		obsList = FXCollections.observableArrayList();
		// current user
		UserSystem currUser = LoginUIController.getCurrUser(); 
		
		Iterator<UserSystem.Album> albumInter = currUser.albumIterator();
		
		while(albumInter.hasNext()) 
		{ 
			
			obsList.add(albumInter.next());				
		}					
		albumList.setItems(obsList);	
		albumList.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);	
		
		name.setCellValueFactory(new PropertyValueFactory<>("albumName"));photo.setCellValueFactory(new PropertyValueFactory<>("numOfPhotos"));
		date.setCellValueFactory(new PropertyValueFactory<>("dateRange"));
		name.setResizable(false);	photo.setResizable(false);	date.setResizable(false);
		name.setSortable(false);	photo.setSortable(false);	date.setSortable(false);
		
		name.setStyle( "-fx-alignment: CENTER;");
		photo.setStyle( "-fx-alignment: CENTER;");
		date.setStyle( "-fx-alignment: CENTER;");
		
		if(!obsList.isEmpty()) {  // if list is not empty; select first item
			
			albumList.getSelectionModel().select(0);
		}
	}
	/**
	 * @return returns index of album that is open
	 */
	public static int getOpenAlbumIndex() {
		
		return openAlbumIndex;
	}
	
	/**
	 * method setCurrUser preps header for albumlistcontroller
	 * @param String u
	 */
	
	public void setCurrUser(String u) {
		titleuser.setText("Welcome, "+ u +".");
			System.out.print(u);
		}
	
	/**
	 * method triggeredLogout returns to login page
	 * @param ActionEvent e
	 */
	
	public void triggeredLogout(ActionEvent e){
		try{
			main.tgrdLogout(e);
		}
		catch(Exception r){
			r.printStackTrace();
		}
	}
	
	/**
	 * @param e takes in an ActionEvent
	 */
	public void triggeredAdd(ActionEvent e) {		
		
	//	int userIndex = LoginUIController.getUserIndex();
		
	String albumName = addBox();  // pop up window for album name
		//String albumName = albumAdded.getText();
	//System.out.println("Album: " + albumName);
		UserSystem currUser = LoginUIController.getCurrUser();
		Iterator<UserSystem.Album> albumIter = currUser.albumIterator();
		
		if(albumName == null) {  // invalid; duplicate album
			
			return; 
		}
		
		UserSystem.Album album = new UserSystem.Album(albumName);  // create new album instance
				
		//UserSystem currUser = LoginUIController.getCurrUser();
	
		currUser.addAlbum(album);
		//if(obsList.isEmpty()) {System.out.println("LISTTT");
		obsList.add(album);  // insert album in observable list
		
		
		//System.out.println("LISTTT" + obsList);
		
		albumList.getSelectionModel().select(obsList.size()-1);  // selects the last album to be inserted into list

		//albumAdded.clear();*/
	}
public static String addBox() {
		
		TextInputDialog addDialog = new TextInputDialog();
		addDialog.setTitle("Add an Album");
		addDialog.setHeaderText("Create Album");
		addDialog.setContentText("Album Name:");
		Alert error = new Alert(AlertType.ERROR);
		Optional<String> result = addDialog.showAndWait();
		
		if(result.isPresent()) {
			
			if(result.get().isEmpty()) {
				
				
				error.setTitle("Input Error");
				error.setContentText("Must Enter Album Name");
				error.show();
				
				return null;
			}
			
			UserSystem currUser = LoginUIController.getCurrUser();
			
			Iterator<UserSystem.Album> albumIter = currUser.albumIterator();
			
			while(albumIter.hasNext()) {
				
				
				if(result.get().equals(albumIter.next().getAlbumName())) {
					
					error.setTitle("Input Error");
					error.setContentText("Existing Album");
					error.show();
					
					return null;
					
				}
				
			}
			
			
			return result.get().trim();
		}
		
		return null;
		
	}
	/**
	 * @param e takes in an ActionEvent
	 */
	public void triggeredDel(ActionEvent e) {
		
		if(obsList.isEmpty()) {
			
			return;
		}
		
		UserSystem.Album selectedAlbum = albumList.getSelectionModel().getSelectedItem();  // gets title of song that is selected
		
		int i = 0;
		for(UserSystem.Album a: obsList) {  // find song in observable list
			
			if(selectedAlbum.toString().equals(a.toString())) {  
				
				break;
				
			}
			
			i++;
		}
		
		UserSystem currUser = LoginUIController.getCurrUser();
		
		currUser.delAlbum(i);
		
		obsList.remove(i);
		
		if(obsList.size() == i) {  // if last item in list is deleted
			
			albumList.getSelectionModel().select(--i);  // select previous item in list
			
		} else {
			
			albumList.getSelectionModel().select(i);  // select next item in list
			
		}
		
		
		
	}
	 /*
	 public int determineAlbumAvailability(String givenName){
			if(albumList1 == null) {
				return -1;
			}
			for(int i = 0; i < albumList1.size(); i++){
				if(albumList1.get(i).receiveName()==null) {
					return i;
				}
				else if(albumList1.get(i).receiveName().equals(givenName)){
					return i;
				}
			}
			return -1;
		}
	 */
	/**
	 * @param e takes in an ActionEvent
	 */
	public void triggeredEdit(ActionEvent e) {
		
		if(obsList.isEmpty()) {
			
			return;
		}
		
		int userIndex = LoginUIController.getUserIndex();
		
		String albumName = editBox();  // pop up window for album name
		
		if(albumName == null) { // invalid; duplicate album
			
			return;
		}
				
		int selectedIndex = albumList.getSelectionModel().getSelectedIndex();
				
		UserSystem currUser = LoginUIController.getCurrUser();
		currUser.getAlbum(selectedIndex).setAlbumName(albumName);
		
		
		albumList.getColumns().get(selectedIndex).setVisible(false);  // refresh columns to display new value
		albumList.getColumns().get(selectedIndex).setVisible(true);
		
		albumList.getSelectionModel().select(selectedIndex);  // select album that was edited
		
	}
public static String editBox() {
		
		TextInputDialog addDialog = new TextInputDialog();
		addDialog.setTitle("Edit Album");
		addDialog.setHeaderText("Edit Album Name");
		addDialog.setContentText("Album Name:");
		
		Optional<String> result = addDialog.showAndWait();
		
		if(result.isPresent()) {
			
			if(result.get().isEmpty()) 
			{
				
				Alert error = new Alert(AlertType.ERROR);
				error.setTitle("Input Error");
				error.setContentText("Must Enter Album Name");
				error.show();
				
				return null;
			}
			UserSystem currUser = LoginUIController.getCurrUser();
			
			Iterator<UserSystem.Album> albumIter = currUser.albumIterator();
			
			while(albumIter.hasNext()) {
				
				
				if(result.get().equals(albumIter.next().getAlbumName())) {
					
					Alert error = new Alert(AlertType.ERROR);
					error.setTitle("Input Error");
					error.setContentText("Existing Album");
					error.show();
					
					return null;
					
				}
				
			}
			
			return result.get().trim();
		}
		
		return null;
	}
	
	
	/**
	 * @param e takes in an ActionEvent
	 * @throws IOException 
	 */
	public void triggeredChoose(ActionEvent e) throws IOException
	{
		if(obsList.isEmpty()) {
			
			return;
		}
		
		openAlbumIndex = albumList.getSelectionModel().getSelectedIndex();
		
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(getClass().getResource("/view/gallery.fxml"));
		AnchorPane root = (AnchorPane) loader.load();
		
		Scene scene = new Scene(root);
		Stage primaryStage = (Stage) choose.getScene().getWindow();  // gets the current stage 
		primaryStage.setScene(scene);
		primaryStage.setResizable(false);
		primaryStage.show();
		
	}
	
	
	/**
	 * @param e takes in an Action Event
	 * @throws IOException throws exception if scene cannot be loaded 
	 */
	public void triggeredSearch(ActionEvent e) throws IOException {
		
		boolean validInput = searchBox();
		
		if(!validInput) 
		{
			return;
		}
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(getClass().getResource("/view/AlbumSearch.fxml"));
		AnchorPane root = (AnchorPane) loader.load();
		
		Scene scene = new Scene(root);
		Stage primaryStage = (Stage) logout.getScene().getWindow();  // gets the current stage 
		primaryStage.setScene(scene);
		primaryStage.setResizable(false);
		primaryStage.show();
	}
public static boolean searchBox() {
		
	SearchController.i_dateTag = null;
	SearchController.f_dateTag = null;
	SearchController.personTag = null;
	SearchController.locTag = null;
		Dialog<ButtonType> dialog = new Dialog<>();
		
		dialog.setTitle("Search Photos");
		dialog.setHeaderText("All Fields are Optional");
		dialog.setResizable(true);

		Label label1 = new Label("Person Tag: ");
		Label label2 = new Label("Location Tag: ");
		Label fromDate = new Label("Date Range: ");
		//Label toDate = new Label("Date: ");
		TextField personTag = new TextField();
		TextField locTag = new TextField();
		Label to = new Label("   to   ");
		
		DatePicker fromD = new DatePicker();
		DatePicker toD = new DatePicker();

		GridPane grid = new GridPane();
		Alert error = new Alert(AlertType.ERROR);
		grid.add(label1, 1, 1);
		grid.add(personTag, 2, 1);
		grid.add(label2, 1, 2);
		grid.add(locTag, 2, 2);
		grid.add(fromDate, 1, 3);
		grid.add(fromD, 2, 3);
		grid.add(to, 3, 3);
		grid.add(toD, 5, 3);
		
		grid.setVgap(15);
		dialog.getDialogPane().setContent(grid);
				
		ButtonType buttonTypeOk = new ButtonType("Okay", ButtonData.OK_DONE);
		dialog.getDialogPane().getButtonTypes().add(buttonTypeOk);
		
		ButtonType buttonTypeCancel = new ButtonType("Cancel", ButtonData.CANCEL_CLOSE);
		dialog.getDialogPane().getButtonTypes().add(buttonTypeCancel);
		
		
		Optional<ButtonType> result = dialog.showAndWait();
		
		if(result.get() == buttonTypeCancel) {
			
			return false;
		}
				
		if(		personTag.getText().isEmpty()
				&& locTag.getText().isEmpty() 
				&& fromD.getValue() == null 
				&& toD.getValue() == null) {
			
			error.setTitle("Input Error");
			error.setContentText("Must enter search criteria");
			error.show();
			
			return false;
		}
		
		SearchController.personTag = personTag.getText().trim();
		SearchController.locTag = locTag.getText().trim();
		
		if(fromD.getValue() != null)
		{
			
			SearchController.i_dateTag = fromD.getValue().format(DateTimeFormatter.ofPattern("MM/dd/yyy"));		
		}	
		if(toD.getValue() != null)
		{
			
			SearchController.f_dateTag = toD.getValue().format(DateTimeFormatter.ofPattern("MM/dd/yyy"));
			
		}	
 		return true;		
	}
	
	/* add.setOnAction((EventHandler<ActionEvent>) new EventHandler<ActionEvent>()
	 {
        @Override
	 public void handle(ActionEvent e){
			if(albumAdded.getText().trim().isEmpty()){
				Alert emptyWarn=new Alert(AlertType.ERROR);
				emptyWarn.setTitle("Input Necessary");
				emptyWarn.setContentText("Please enter a title: ");
				emptyWarn.show();
			}
			else if((determineAlbumAvailability(albumAdded.getText())!=-1)){
				Alert dupWarn=new Alert(AlertType.ERROR);
				dupWarn.setTitle("Duplicate input");
				dupWarn.setContentText("album title already exists");
				dupWarn.show();
			}
			else{
				UserSystem newEntry = new UserSystem(albumAdded.getText());
				if(albumList1==null) {
					albumList1=new ArrayList<UserSystem>();
				}
				albumList1.add(newEntry);
				start();
				albumAdded.clear();
			
			}
        }
		});*/

	
}
