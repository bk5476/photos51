package view;

/**
 * @author Wilson Peralta
 * @author Beomsik Kim
 */

import java.io.File;

import java.io.IOException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ChoiceDialog;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextInputDialog;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import app.Database;
import app.UserSystem;

public class GalleryController implements Initializable
{
	private static final int INVALDFILEPATH = 0;
	private static final int NOINPUT = 1;
	private static final int SAMETAG = 2;
	private static final int NONEXISTANTTAG = 3;
	
	UserSystemController main = new UserSystemController();
	
	@FXML private ListView<UserSystem.Album.Photo> photoList;
	@FXML private ImageView img;
	@FXML private Button addPhoto;
	@FXML private Button addTag;
	@FXML private Button move;
	@FXML private Button goBack;
	@FXML private Button logout;
	
	@FXML Label personTagText;
	@FXML Label locationTagText;
	@FXML Label captionText;
	@FXML Label dateText;
	
	private static ObservableList<UserSystem.Album.Photo> obsList;

	
	private static final String SPLIT = "@@";
	/**
	 * Initializes the scene by populating the ListView and displaying an Image in the ImageView
	 */
	@Override
	public void initialize(URL location, ResourceBundle resources)
	{
		obsList = FXCollections.observableArrayList();
		
		UserSystem currentUserSystem = LoginUIController.getCurrUser();		
		UserSystem.Album currentAlbum = currentUserSystem.getAlbum(AlbumlistController.getOpenAlbumIndex());
		
		Iterator<UserSystem.Album.Photo> photoIter = currentUserSystem.getAlbum(AlbumlistController.getOpenAlbumIndex()).photoIterator();
		
		boolean invalidFileFlag = false;
		
		while(photoIter.hasNext())
		{
			UserSystem.Album.Photo photo = photoIter.next();
			File file = new File(photo.getPhotoLocation());
			
			if (file.exists())
			{
				obsList.add(photo);
			} else {
				invalidFileFlag = true;
				photoIter.remove();
				currentAlbum.opNumOfPhotos(1, '-');
			}
		}
		
		photoList.setItems(obsList);
		
		blankDisplay();
		photoList.getSelectionModel().selectedItemProperty().addListener((obs, oldVal, newVal) -> updateDisplay());
		
		photoList.getSelectionModel().select(0);
		
		updateDisplay();
		
		if (invalidFileFlag)
		{
			errorPopup(INVALDFILEPATH);
		}
		
	}
	public static String getCharSplit()
	{
		return SPLIT;
	}

	/**
	 * Updates the photo display with the image, caption, date and tags
	 */
	private void updateDisplay()
	{
		if(!obsList.isEmpty())
		{
			UserSystem.Album.Photo photo = photoList.getSelectionModel().getSelectedItem();	
			File file = new File(photo.getPhotoLocation());

			if (file.exists()) //check if photo still exists at its location on computer
			{
				Image display = new Image(file.toURI().toString());
				img.setImage(display);
				captionText.setText(photo.getCaption());
				SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
				dateText.setText(sdf.format(photo.getPhotoDate()));
				personTagText.setText(photo.getTags("person"));
				locationTagText.setText(photo.getTags("location"));
			} else {
				errorPopup(INVALDFILEPATH);
				deletePhoto();
				
				if (obsList.isEmpty())
				{
					blankDisplay();
				} else {
					updateDisplay();
				}
			}
		} else {
			blankDisplay();
		}
	}
	
	/**
	 * Default display - a blank image and blank labels
	 */
	public void blankDisplay()
	{
		img.setImage(null);
		captionText.setText("");
		dateText.setText("");
		personTagText.setText("");
		locationTagText.setText("");
	}
	
	/**
	 * Adds a photo to an album
	 * @param e
	 * @throws IOException
	 * @throws ParseException
	 */
	
	public void triggeredLogout(ActionEvent e){
		try{
			main.tgrdLogout(e);
		}
		catch(Exception r){
			r.printStackTrace();
		}
	}
	public void triggeredAddPhoto(ActionEvent e) throws IOException, ParseException
	{
		FileChooser addPhoto = new FileChooser();
		configureFileChooser(addPhoto);
		File file = addPhoto.showOpenDialog(null);
		
		if (file != null)
		{
			UserSystem.Album.Photo image = new UserSystem.Album.Photo(file.getName(), file.toString());
			//System.out.println(image);
			SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
			
			String s = sdf.format(file.lastModified());
			
			Date date = sdf.parse(s);
			
			image.setPhotoDate(date); 
		
			UserSystem currentUserSystem = LoginUIController.getCurrUser();	
			UserSystem.Album currentAlbum = currentUserSystem.getAlbum(AlbumlistController.getOpenAlbumIndex());
			System.out.println("HERE Album:" + currentAlbum);
			currentAlbum.addPhoto(image);
		
			obsList.add(image);
		
			photoList.getSelectionModel().select(obsList.size() - 1);
		
			updateDisplay();
			
			if(obsList.size() == 1) {
				uptoDate(currentAlbum, image);
				return;
			}
			
			updateDateRange(currentAlbum);
		}
	}
	
	/**
	 * Configures the file chooser with custom settings
	 * @param fc
	 */
	private static void configureFileChooser(final FileChooser fc)
	{
		fc.setTitle("Add a Photo");
		
		/*initial directory set to UserSystem's home directory*/
		fc.setInitialDirectory(new File(System.getProperty("user.home")));
		
		/*allow UserSystems to search for different file extensions when adding photos*/
		fc.getExtensionFilters().addAll(
				new FileChooser.ExtensionFilter("All Images", "*.*"),
				new FileChooser.ExtensionFilter("JPG", "*.jpg"),
				new FileChooser.ExtensionFilter("PNG", "*.png"));
	}
	
	/**
	 * method triggeredRemove
	 * Deletes a photo from an album and updates its display on app
	 * @param E ActionEvent
	 * 
	 */
	public void triggeredRemove(ActionEvent e) throws IOException
	{
		if(!obsList.isEmpty())
		{
			deletePhoto();
		
			updateDisplay();
		}
		
		
		
		UserSystem currentUserSystem = LoginUIController.getCurrUser();	
		UserSystem.Album currentAlbum = currentUserSystem.getAlbum(AlbumlistController.getOpenAlbumIndex());

		updateDateRange(currentAlbum);
		
	
	}
	
	/**
	 * method deletePhoto deletes photo from List structure
	 */
	public void deletePhoto()
	{
		UserSystem.Album.Photo photoToDelete = photoList.getSelectionModel().getSelectedItem();
		
		UserSystem currentUserSystem = LoginUIController.getCurrUser();	
		UserSystem.Album currentAlbum = currentUserSystem.getAlbum(AlbumlistController.getOpenAlbumIndex());
		currentAlbum.deletePhoto(photoToDelete);

		obsList.remove(photoToDelete);
	}
	
	/**
	 * method triggeredCopy copies an image from an album to another
	 * @param e ActionEvent
	 */
	public void triggeredCopy(ActionEvent e)
	{
		UserSystem.Album.Photo photoToMove = photoList.getSelectionModel().getSelectedItem();
		UserSystem currentUserSystem = LoginUIController.getCurrUser();	
		UserSystem.Album destAlbum;
		
		List<String> albumChoices = new ArrayList<>();
		
		Iterator<UserSystem.Album> albumInter = currentUserSystem.albumIterator();
		
		String albumName = null;
		
		while(albumInter.hasNext())
		{
			UserSystem.Album album = albumInter.next();
			if (album != currentUserSystem.getAlbum(AlbumlistController.getOpenAlbumIndex()))
			{
				albumChoices.add(album.getAlbumName());
			}
		}
		
		if (albumChoices.size() > 0)
		{
			albumName = moveBox("copy", albumChoices);
			
			if (albumName != null)
			{
				albumInter = currentUserSystem.albumIterator();
				destAlbum = albumSearch(albumName, albumInter);
			
				destAlbum.addPhoto(photoToMove);
				updateDateRange(destAlbum);
			
				updateDisplay();
			}
		}
	}
	
	/**
	 *method triggeredMove moves an image from an album to another. 
	 *it copies the image and the deletes the original.
	 * @param e ActionEvent
	 */
	public void triggeredMove(ActionEvent e)
	{
		UserSystem.Album.Photo photoToMove = photoList.getSelectionModel().getSelectedItem();
		
		UserSystem currentUserSystem = LoginUIController.getCurrUser();	
		UserSystem.Album currentAlbum = currentUserSystem.getAlbum(AlbumlistController.getOpenAlbumIndex());
		UserSystem.Album destAlbum;
		
		List<String> albumChoices = new ArrayList<>();
		
		Iterator<UserSystem.Album> albumInter = currentUserSystem.albumIterator();
		
		String albumName = null;
		
		while(albumInter.hasNext())
		{
			UserSystem.Album album = albumInter.next();
			if (album != currentUserSystem.getAlbum(AlbumlistController.getOpenAlbumIndex()))
			{
				albumChoices.add(album.getAlbumName());
			}
		}
		
		if (albumChoices.size() > 0)
		{
			albumName = moveBox("move", albumChoices);
			
			if (albumName != null)
			{
				albumInter = currentUserSystem.albumIterator();
				destAlbum = albumSearch(albumName, albumInter);
			
				destAlbum.addPhoto(photoToMove);
				updateDateRange(destAlbum);
			
				currentAlbum.deletePhoto(photoToMove);
				updateDateRange(currentAlbum);
				obsList.remove(photoToMove);
			
				updateDisplay();
			}
		}
	}
	
	/**
	 * Dialog popup used to copy/move an image between albums
	 * @param keyword
	 * @param list
	 * @return the album the UserSystem moves/copies an image to
	 */
	public static String moveBox(String keyword, List<String> list)
	{
		ChoiceDialog<String> moveDialog = new ChoiceDialog<String>(list.get(0), list);
		Optional<String> result;
		
		if (keyword == "move")
		{
			moveDialog.setTitle("Move a Photo");
			moveDialog.setHeaderText("Choose an album to move your photo to.");
			moveDialog.setContentText("Albums:");
		
			result = moveDialog.showAndWait();
			
			if (result.isPresent())
			{
				return result.get().trim();
			}
		} else if (keyword == "copy") {
			moveDialog.setTitle("Copy a Photo");
			moveDialog.setHeaderText("Choose an album to copy your photo to.");
			moveDialog.setContentText("Albums:");
		
			result = moveDialog.showAndWait();
			
			if (result.isPresent())
			{
				return result.get().trim();
			}
		}
		
		return null;
	}
	
	public UserSystem.Album albumSearch(String key, Iterator<UserSystem.Album> iter)
	{
		while(iter.hasNext())
		{
			UserSystem.Album album = iter.next();
			if (album.getAlbumName().equals(key))
			{
				return album;
			}
		}
		
		return null;
	}
	
	/**
	 * method triggeredAddTag Adds a tag to an image
	 * @param e ActionEvent
	 */
	public void triggeredAddTag(ActionEvent e)
	{
		String tag = tagTypeBox("add");
		
		if (tag != null)
		{
			UserSystem.Album.Photo photo = photoList.getSelectionModel().getSelectedItem();
			
			String[] args = tag.split(SPLIT);
			
			if(photo.addTag(args[0], args[1]))
			{
				updateDisplay();
			} else {
				errorPopup(SAMETAG);
			}
		}
	}
	
	public static String editBox(String keyword)
	{
		TextInputDialog addDialog = new TextInputDialog();
		Optional<String> result;
		
		if (keyword == "person" || keyword == "location")
		{
			addDialog.setTitle("Tag Editing");
			addDialog.setHeaderText("Add a " + keyword + " Tag");
			addDialog.setContentText("Enter Tag:");
			
			result = addDialog.showAndWait();
			
			if(result.isPresent())
			{
				if(result.get().isEmpty())
				{
					errorPopup(NOINPUT);
					
					return null;
				}
				
				return keyword + SPLIT + result.get().trim();
			}
		} else if (keyword == "caption") {
			addDialog.setTitle("Caption Editing");
			addDialog.setHeaderText("Edit your " + keyword);
			addDialog.setContentText("Enter Caption:");
			
			result = addDialog.showAndWait();
			
			if(result.isPresent())
			{
				return result.get().trim();
			}
		}
		
		return null;
		
	}
	
	/**
	 * method tagTypeBox ask user what type of tags user wishes to apply 
	 * @param operation
	 * @return the tag the UserSystem wishes to edit
	 */
	public static String tagTypeBox(String operation)
	{
		Alert alert = new Alert(AlertType.CONFIRMATION);
		alert.setTitle("Tag Editing");
		alert.setHeaderText("Tag Type");
		
		ButtonType personButton = new ButtonType("Person"), 
				locationButton = new ButtonType("Location"),
				cancelButton = new ButtonType("Cancel", ButtonData.CANCEL_CLOSE);
		
		alert.getButtonTypes().setAll(personButton, locationButton, cancelButton);
		
		if (operation == "add")
		{
			alert.setContentText("Select the type of tag you want to add.");

			Optional<ButtonType> result = alert.showAndWait();
			if (result.get() == personButton)
			{
			    return editBox("person");
			} else if (result.get() == locationButton) {
				return editBox("location");
			} else {
			    // ... UserSystem chose CANCEL or closed the dialog
			}
		} else if (operation == "delete") {
			alert.setContentText("Select the type of tag you want to delete.");

			Optional<ButtonType> result = alert.showAndWait();
			if (result.get() == personButton)
			{
				return editBox("person");
			} else if (result.get() == locationButton) {
				return editBox("location");
			} else {
			    // ... UserSystem chose CANCEL or closed the dialog
			}
		}
		
		return null;
	}
	
	/**
	 * method triggeredRemoveTag
	 * Removes a tag from a photo
	 * @param e ActionEvent
	 */
	public void triggeredRemoveTag(ActionEvent e)
	{
		String tag = tagTypeBox("delete");
		
		if (tag != null)
		{
			UserSystem.Album.Photo photo = photoList.getSelectionModel().getSelectedItem();
			
			String[] args = tag.split(SPLIT);
			
			System.out.println(args[0] + "," + args[1]);

			if(photo.removeTag(args[0], args[1]))
			{
				updateDisplay();
			} else {
				errorPopup(NONEXISTANTTAG);
			}
		}
	}
	
	/**
	 * method triggeredcaptionOptions
	 * @param e ActionEvent
	 */
	public void triggeredCaption(ActionEvent e)
	{
		Alert alert = new Alert(AlertType.CONFIRMATION);
		alert.setTitle("Caption Options");
		alert.setHeaderText("Tag Type");
		
		ButtonType editButton = new ButtonType("Edit Caption");
		ButtonType removeButton = new ButtonType("Remove Caption");
		ButtonType cancelButton = new ButtonType("Cancel", ButtonData.CANCEL_CLOSE);
		
		alert.getButtonTypes().setAll(editButton, removeButton, cancelButton);
		
		Optional<ButtonType> result = alert.showAndWait();
		if (result.get() == editButton)
		{
		    String caption = editBox("caption");
		    
			if (caption != null)
			{
				UserSystem.Album.Photo photo = photoList.getSelectionModel().getSelectedItem();
				
				photo.setCaption(caption);
				updateDisplay();
			}
		} else if (result.get() == removeButton) {
			UserSystem.Album.Photo photo = photoList.getSelectionModel().getSelectedItem();
			
			if (photo.getCaption() != null)
			{
				photo.setCaption("");
				updateDisplay();
			}
		} else {
		    // ... UserSystem chose CANCEL or closed the dialog
		}
	}
	
	/**
	 * Returns to the previous scene - in this case, the Album scene
	 * @param e
	 * @throws IOException
	 */
	public void triggeredBack(ActionEvent e) throws IOException
	{
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(getClass().getResource("/view/Albumlist.fxml"));
		BorderPane root = (BorderPane) loader.load();
		
		Scene scene = new Scene(root);
		Stage primaryStage = (Stage) goBack.getScene().getWindow();  // gets the current stage 
		primaryStage.setScene(scene);
		primaryStage.setResizable(false);
		primaryStage.show();
	}
	
	
	/**
	 * Adjusts the date range of an album - single photo version
	 * @param album
	 * @param img
	 */
	public static void uptoDate(UserSystem.Album album, UserSystem.Album.Photo img)
	{
		album.setBeginDate(img.getPhotoDate());
		album.setEndDate(img.getPhotoDate());
	}
	
	/**
	 * Adjusts the date range of an album
	 * @param album
	 */
	@SuppressWarnings("deprecation")
	public static void updateDateRange(UserSystem.Album album)
	{
		Iterator<UserSystem.Album.Photo> photoIter = album.photoIterator();
		UserSystem.Album.Photo photo;
		
		if(!photoIter.hasNext())
		{
			album.setBeginDate(null);
			album.setEndDate(null);
		} else {
			if (album.getBeginDate() == null || album.getEndDate() == null)
			{
				photo = photoIter.next();
				uptoDate(album, photo);
			} else {
				album.setBeginDate(new Date());
				
				Date endDate = new Date();
				endDate.setYear(0);
				album.setEndDate(endDate);
				
				while(photoIter.hasNext())
				{
					photo = photoIter.next();
					if(photo.getPhotoDate().before(album.getBeginDate()))
					{				
						album.setBeginDate(photo.getPhotoDate());
					}
			
					if(photo.getPhotoDate().after(album.getEndDate())) 
					{		
						album.setEndDate(photo.getPhotoDate());
					}
				}
			}
		}
	}
	
	/**
	 * Shows an error popup depending on the error code passed in
	 * @param errorCode
	 */
	public static void errorPopup(int errorCode)
	{
		Alert error = new Alert(AlertType.ERROR);
		
		switch (errorCode)
		{
			case INVALDFILEPATH /*image does not exist at file path anymore*/:
				error.setTitle("Invalid File Path");
				error.setContentText("The file path is invalid for one or more photos. Photo information for those photos was deleted from application.");
				break;
			case NOINPUT /*tag input error*/:
				error.setTitle("Tag Input Error");
				error.setContentText("No tag was entered. Please enter in a tag.");
				break;
			case SAMETAG /*add tag same tag error*/:
				error.setTitle("Same Tag Error");
				error.setContentText("Duplicate tag detected. Tag not added.");
				break;
			case NONEXISTANTTAG /*remove tag error*/:
				error.setTitle("Invalid Tag Error");
				error.setContentText("Entered tag does not exist.");
				break;
			default:
				error.setTitle("Default error.");
				error.setContentText("An incorrect error code was called. Sorry about that!");
				break;
		}
		
		error.show();
	}
}