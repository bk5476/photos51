
package view;
/**
 * @author Wilson Peralta
 * @author Beomsik Kim
 */
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert;
import javafx.scene.layout.AnchorPane;
import javafx.scene.control.Alert.AlertType;
import java.util.ArrayList;
import java.util.Optional;
import app.UserSystem;
import view.UserSystemController;


/**
 * Class AdminUIController controls AdminUI javafx page
 * @param userList1 arraylist of UserSystem that keeps track of active users
 * @param strol observablelist from javafx for active users
 */
public class AdminUIController {
	public static ArrayList<UserSystem> userList1;
	private ObservableList<String> strol;
	
	@FXML Button add;
	@FXML ListView<String> ulv;
	@FXML AnchorPane uap;
	@FXML Button logout;
	@FXML TextField userToBeAdded;
	@FXML Button delete;
	
	UserSystemController system1=new UserSystemController();
	
	/**
	 * method retrieveUL retrieves userList1
	 * @return userList1
	 */
	public static ArrayList<UserSystem> retrieveUL(){
		return userList1;
	}
	
	/**
	 * method determineUserAvailability checks if username in textbox is in userList1
	 * @param string givenName
	 * @return the index of userList1 where givenName is found, or -1 is not found
	 */
	public int determineUserAvailability(String givenName){
		if(userList1 == null) {
			return -1;
		}
		for(int i = 0; i < userList1.size(); i++){
			if(userList1.get(i).receiveName()==null) {
				return i;
			}
			else if(userList1.get(i).receiveName().equals(givenName)){
				return i;
			}
		}
		return -1;
	}
	/**
	 * method prepMain prepares AdminUIController for main controller
	 * @param UserSystemController mainControl
	 */
	
	public void prepMain(UserSystemController mainControl){
		system1=mainControl;
	}
	
	public void triggeredLogout(ActionEvent e){
		try{
			system1.tgrdLogout(e);
		}
		catch(Exception r){
			r.printStackTrace();
		}
	}
	
	/**
	 * method triggeredDelete deletes selected user
	 * @param ActionEvent e
	 */
	
	
	public void triggeredDelete(ActionEvent e){
		if(strol.isEmpty()){
			return;
		}
		else{
			Alert delWarn = new Alert(AlertType.CONFIRMATION);
			delWarn.setTitle("User Delete Confirm");
			delWarn.setContentText("Confirm deletion of user: "+ulv.getSelectionModel().getSelectedItem()+"?");
			Optional<ButtonType> response = delWarn.showAndWait();
			if(response.get() == ButtonType.OK){
				int i3 = determineUserAvailability(ulv.getSelectionModel().getSelectedItem());
				userList1.remove(i3);
				start();
			}
		}
	}
	
	/**
	 * method triggeredAdd adds new user
	 * @param ActionEvent e
	 */
	
	public void triggeredAdd(ActionEvent e){
		if(userToBeAdded.getText().trim().isEmpty()){
			Alert emptyWarn=new Alert(AlertType.ERROR);
			emptyWarn.setTitle("Input Necessary");
			emptyWarn.setContentText("Please enter username");
			emptyWarn.show();
		}
		else if((determineUserAvailability(userToBeAdded.getText())!=-1)||userToBeAdded.getText().equalsIgnoreCase("admin")){
			Alert dupWarn=new Alert(AlertType.ERROR);
			dupWarn.setTitle("Duplicate input");
			dupWarn.setContentText("User already exists");
			dupWarn.show();
		}
		else{
			UserSystem newEntry = new UserSystem(userToBeAdded.getText());
			if(userList1==null) {
				userList1=new ArrayList<UserSystem>();
			}
			userList1.add(newEntry);
			start();
			userToBeAdded.clear();
		}
	}
	
	/**
	 * method start prepares and loads users into observablelist
	 */

	public void start() {
		 if(userList1==null) {
			 userList1=new ArrayList<UserSystem>();
		 }
		 strol=FXCollections.observableArrayList();
		 for(int i = 0; i<userList1.size(); i++){
			 if(userList1.isEmpty()) break;
			 strol.add(userList1.get(i).receiveName());
		 }
		 ulv.setItems(strol);
		 ulv.getSelectionModel().select(0);
		 ulv
	 	.getSelectionModel()
	 	.selectedItemProperty();
	}
}